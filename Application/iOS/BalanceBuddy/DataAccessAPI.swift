//
//  DataAccessAPI.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/7/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit
import CoreData

class DataAccessAPI: NSObject {
    class var SharedInstance : DataAccessAPI {
        struct Singleton {
            static let instance = DataAccessAPI()
        }
        return Singleton.instance
    }
    
    // get AppDelegate instance
    var appDelegate : AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    
    // Function to get Countries
    func getCountries(forceUpdate: Bool! = false, completion: (countries : [Country]?, error: NSError?) -> ()){
        var countriesData : [Country] = []
        var error : NSError?
        
        if let ctx : NSManagedObjectContext = appDelegate.managedObjectContext {
            
            // 1. Get local data
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Country")
            fetch.returnsObjectsAsFaults = false
            
            var results : [Country] = ctx.executeFetchRequest(fetch, error: &error) as! [Country]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                completion(countries: countriesData, error: error)
            }
            else {
                if results.count > 0 && forceUpdate == false {
                    CommonAPI.sharedInstance.log("Returning local countries data", type: CommonAPI.LogType.Information)
                    countriesData = results as [Country]
                    completion(countries: countriesData, error: error)
                }
                else {
                    CommonAPI.sharedInstance.log("Get countries data from server", type: CommonAPI.LogType.Information)
                    
                    if forceUpdate == true {
                        CommonAPI.sharedInstance.log("Emptying local countries(\(results.count)) data", type: CommonAPI.LogType.Information)

                        // empty the local data
                        for singleCountry in results {
                            ctx.deleteObject(singleCountry)
                        }
                        
                    }
                    // Get the data from server
                    
                    var query: PFQuery = PFQuery(className: "Countries")
                    query.orderByAscending("Country")
                    query.findObjectsInBackgroundWithBlock { (results: [AnyObject]?, error: NSError?) -> Void in
                        
                        if error == nil {
                            
                            var fetchedCountries: [PFObject] = results as! [PFObject]
                            
                            CommonAPI.sharedInstance.log("Got countries data(\(fetchedCountries.count)) from server", type: CommonAPI.LogType.Information)
                            
                            for fetchedCountry in fetchedCountries {
                                // Insert locally
                                var country: Country = NSEntityDescription.insertNewObjectForEntityForName("Country", inManagedObjectContext: ctx) as! Country
                                country.objectId = fetchedCountry.valueForKey("objectId") as! String
                                country.countryName = fetchedCountry.valueForKey("Country") as! String
                                country.updatedAt = fetchedCountry.valueForKey("updatedAt") as! NSDate
                                country.isSelected = false
                                
                                // Add to the collection
                                countriesData.append(country)
                                
                                ctx.save(nil)
                                
                            } // process fetchedCountries
                            
                            completion(countries: countriesData, error: nil)
                        } // if error
                        else {
                            CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                            completion(countries: countriesData, error: error)
                        }
                    }
                }
            }
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            completion(countries: countriesData, error: NSError(domain: NSBundle.mainBundle().bundleIdentifier!, code: 100, userInfo: nil))
        }
    }
    
    // Function to get available carriers for a country
    func getCarriers(country: Country, forceUpdate: Bool! = false, completion: (carriers : [Carrier]?, error: NSError?) -> ()){
        var carriersData : [Carrier] = []
        var error : NSError?
        
        if let ctx : NSManagedObjectContext = appDelegate.managedObjectContext {
            
            // 1. Get local data
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Carrier")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "countryRel = %@", country)
            
            var results : [Carrier] = ctx.executeFetchRequest(fetch, error: &error) as! [Carrier]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                completion(carriers: carriersData, error: error)
            }
            else {
                if results.count > 0 && forceUpdate == false {
                    CommonAPI.sharedInstance.log("Returning local carriers data", type: CommonAPI.LogType.Information)
                    carriersData = results as [Carrier]
                    completion(carriers: carriersData, error: error)
                }
                else {
                    CommonAPI.sharedInstance.log("Get carriers data from server", type: CommonAPI.LogType.Information)
                    
                    if forceUpdate == true {
                        CommonAPI.sharedInstance.log("Emptying local carriers(\(results.count)) data", type: CommonAPI.LogType.Information)
                        
                        // empty the local data
                        for singleCarrier in results {
                            ctx.deleteObject(singleCarrier)
                        }
                        
                    }
                    // Get the data from server
                    
                    var query: PFQuery = PFQuery(className: "Carriers")
                    query.whereKey("Country", equalTo: PFObject(withoutDataWithClassName: "Countries", objectId: country.objectId))
                    query.orderByAscending("CarrierName")
                    query.findObjectsInBackgroundWithBlock { (results: [AnyObject]?, error: NSError?) -> Void in
                        
                        if error == nil {
                            var fetchedCarriers: [PFObject] = results as! [PFObject]
                            for fetchedCarrier in fetchedCarriers {
                                // Insert locally
                                var carrier : Carrier = NSEntityDescription.insertNewObjectForEntityForName("Carrier", inManagedObjectContext: ctx) as! Carrier
                                carrier.objectId = fetchedCarrier.objectId as String!
                                carrier.carrierName = fetchedCarrier.valueForKey("CarrierName") as! String
                                carrier.updatedAt = fetchedCarrier.valueForKey("updatedAt") as! NSDate
                                carrier.isSelected = 0
                                carrier.countryRel = country
                                
                                // Download the image and save it
                                if let imagePFFile = fetchedCarrier.valueForKey("CarrierImage") as! PFFile! {
                                    CommonAPI.sharedInstance.log("Get carrier image URL \(imagePFFile.url)", type: CommonAPI.LogType.Information)
                                    
                                    // Getting data synchronously as this is in a loop
                                    carrier.carrierImageURL =  imagePFFile.url
                                }
                                else {
                                    // set the default image
                                    CommonAPI.sharedInstance.log("No carrier image", type: CommonAPI.LogType.Information)
                                    carrier.carrierImageURL = nil
                                }
                                
                                // add it to collection
                                carriersData.append(carrier)
                                
                                ctx.save(nil)
                                
                            } // process fetchedCarriers
                            completion(carriers: carriersData, error: error)
                            
                        } // if error
                        else {
                            CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                            completion(carriers: carriersData, error: error)
                        }
                    }
                }
            }
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            completion(carriers: carriersData, error: NSError(domain: NSBundle.mainBundle().bundleIdentifier!, code: 100, userInfo: nil))
        }
    }
    
    // Function to get available codes for a carrier
    func getCodes(carrier: Carrier, forceUpdate: Bool! = false, completion: (codes : [Code]?, error: NSError?) -> ()){
        var codesData : [Code] = []
        var error : NSError?
        
        if let ctx : NSManagedObjectContext = appDelegate.managedObjectContext {
            
            // 1. Get local data
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Code")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "carrierRel == %@", carrier)
            
            
            var results : [Code] = ctx.executeFetchRequest(fetch, error: &error) as! [Code]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                completion(codes: codesData, error: error)
            }
            else {
                if results.count > 0 && forceUpdate == false {
                    CommonAPI.sharedInstance.log("Returning local codes data", type: CommonAPI.LogType.Information)
                    codesData = results as [Code]
                    completion(codes: codesData, error: error)
                }
                else {
                    CommonAPI.sharedInstance.log("Get codes data from server", type: CommonAPI.LogType.Information)
                    
                    if forceUpdate == true {
                        CommonAPI.sharedInstance.log("Emptying local codes(\(results.count)) data", type: CommonAPI.LogType.Information)
                        
                        // empty the local data
                        for singleCode in results {
                            ctx.deleteObject(singleCode)
                        }
                        
                    }
                    // Get the data from server
                    
                    var serverQuery = PFQuery(className: "Codes")
                    serverQuery.whereKey("Carrier", equalTo: PFObject(withoutDataWithClassName: "Carriers", objectId: carrier.objectId))
                    serverQuery.findObjectsInBackgroundWithBlock({ (codes: [AnyObject]?, error: NSError?) -> Void in
                        if error == nil && codes != nil && codes!.count > 0 {
                            
                            for code in codes! {
                                let codePFObj = code as! PFObject
                                
                                var code : Code = NSEntityDescription.insertNewObjectForEntityForName("Code", inManagedObjectContext: ctx) as! Code
                                code.code = codePFObj.valueForKey("Code") as! String
                                code.codeDescription = codePFObj.valueForKey("Description") as! String
                                code.isFavouriteByUser = 0
                                code.objectId = codePFObj.objectId!
                                code.updatedAt = codePFObj.valueForKey("updatedAt") as! NSDate
                                code.carrierRel = carrier
                                
                                // add it to carrierCodesList
                                codesData.append(code)
                                
                                ctx.save(nil)
                            } // process codes
                        
                            completion(codes: codesData, error: nil)
                        }
                        else {
                            CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                            completion(codes: codesData, error: error)
                        }
                        
                    })
                    
                }
            }
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            completion(codes: codesData, error: NSError(domain: NSBundle.mainBundle().bundleIdentifier!, code: 100, userInfo: nil))
        }
    }
    
    
    // Function to update Country
    func  updateCountry(country: Country) -> Bool {
        if let ctx = appDelegate.managedObjectContext {
            
            CommonAPI.sharedInstance.log("Updating country \(country.countryName)", type: CommonAPI.LogType.Information)
            
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Country")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "objectId == %@", country.objectId)
            
            var error: NSError?
            var results : [Country] = ctx.executeFetchRequest(fetch, error: &error) as! [Country]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return false
            }
            
            // update
            if var countryResult = results[0] as Country! {
                countryResult = country
                ctx.save(&error)
                
                if error != nil {
                    CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                    return false
                }
                
                CommonAPI.sharedInstance.log("Successfully update country \(country.countryName)", type: CommonAPI.LogType.Information)
                return true
            }
            else {
                CommonAPI.sharedInstance.log("Failed to update country \(country.countryName) as there was no local data with objectId \(country.objectId)", type: CommonAPI.LogType.Warning)
                return false
            }
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return false
        }
    }
    
    // Function to update carrier
    func  updateCarrier(carrier: Carrier) -> Bool {
        if let ctx = appDelegate.managedObjectContext {
            
            CommonAPI.sharedInstance.log("Updating carrier \(carrier.carrierName)", type: CommonAPI.LogType.Information)
            
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Carrier")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "objectId == %@", carrier.objectId)
            
            var error: NSError?
            var results : [Carrier] = ctx.executeFetchRequest(fetch, error: &error) as! [Carrier]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return false
            }
            
            // update
            if var carrierResult = results[0] as Carrier! {
                carrierResult = carrier
                ctx.save(&error)
                
                if error != nil {
                    CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                    return false
                }
                
                CommonAPI.sharedInstance.log("Successfully update carrier \(carrier.carrierName)", type: CommonAPI.LogType.Information)
                return true
            }
            else {
                CommonAPI.sharedInstance.log("Failed to update carrier \(carrier.carrierName) as there was no local data with objectId \(carrier.objectId)", type: CommonAPI.LogType.Warning)
                return false
            }
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return false
        }
    }
    
    // Function to update code
    func  updateCode(code: Code) -> Bool {
        if let ctx = appDelegate.managedObjectContext {
            
            CommonAPI.sharedInstance.log("Updating code \(code.codeDescription)", type: CommonAPI.LogType.Information)
            
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Code")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "objectId == %@", code.objectId)
            
            var error: NSError?
            var results : [Code] = ctx.executeFetchRequest(fetch, error: &error) as! [Code]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return false
            }
            
            // update
            if var codeResult = results[0] as Code! {
                codeResult = code
                ctx.save(&error)
                
                if error != nil {
                    CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                    return false
                }
                
                CommonAPI.sharedInstance.log("Successfully update code \(code.codeDescription)", type: CommonAPI.LogType.Information)
                return true
            }
            else {
                CommonAPI.sharedInstance.log("Failed to update code \(code.codeDescription) as there was no local data with objectId \(code.objectId)", type: CommonAPI.LogType.Warning)
                return false
            }
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return false
        }
    }
    
    // Function to set a favourite code 
    func setFavouriteCode(objectId: String, state: Bool) -> Bool {
        if let ctx = appDelegate.managedObjectContext {
            
            CommonAPI.sharedInstance.log("Favourite code with objectId \(objectId)", type: CommonAPI.LogType.Information)
            
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Code")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "objectId == %@", objectId)
            
            var error: NSError?
            var results : [Code] = ctx.executeFetchRequest(fetch, error: &error) as! [Code]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return false
            }
            
            // update
            if var codeResult = results.last {
                codeResult.setValue(state == true ? 1 : 0, forKey: "isFavouriteByUser")
                ctx.save(&error)
                
                if error != nil {
                    CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                    return false
                }
                
                CommonAPI.sharedInstance.log("Successfully favourited code with objectId \(objectId)", type: CommonAPI.LogType.Information)
                return true
            }
            else {
                CommonAPI.sharedInstance.log("Failed to favourite code as there was no local data with objectId \(objectId)", type: CommonAPI.LogType.Warning)
                return false
            }
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return false
        }
    }
    
    // Function to get selected country
    func getSelectedCountry() -> Country? {
        var selected : Country?
        
        if let ctx = appDelegate.managedObjectContext {
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Country")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "isSelected == 1")
            
            var error: NSError?
            var result = ctx.executeFetchRequest(fetch, error: &error) as! [Country]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return selected!
            }
            
            if result.count > 0 {
                selected = result[0]
            }
            
            return selected
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return selected!

        }
    }
    
    // Function to get selected carrier
    func getSelectedCarrier(country: Country) -> Carrier? {
        var selected : Carrier?
        
        if let ctx = appDelegate.managedObjectContext {
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Carrier")
            fetch.returnsObjectsAsFaults = false
            fetch.predicate = NSPredicate(format: "isSelected == 1 && ANY countryRel = %@", country)
            
            var error: NSError?
            var result = ctx.executeFetchRequest(fetch, error: &error) as! [Carrier]
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return selected!
            }
            
            if result.count > 0 {
                 selected = result[0]
            }
           
            return selected
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return selected!
            
        }

    }
    
    // Function to get favourite codes
    func getFavouriteCodes(carrier: Carrier) -> [Code] {
        var selected : [Code] = []
        
        if let ctx = appDelegate.managedObjectContext {
            var fetch : NSFetchRequest = NSFetchRequest(entityName: "Code")
            fetch.returnsObjectsAsFaults = false
            var isFavPredicate : NSPredicate = NSPredicate(format: "isFavouriteByUser == 1")
            var carrierPredicate : NSPredicate = NSPredicate(format: "carrierRel = %@", carrier)
            let compoundPredicate :NSCompoundPredicate = NSCompoundPredicate.andPredicateWithSubpredicates([carrierPredicate, isFavPredicate])
            fetch.predicate = compoundPredicate
            
            var error: NSError?
            var result = ctx.executeFetchRequest(fetch, error: &error) as! [Code]
            selected = result
            
            if error != nil {
                CommonAPI.sharedInstance.log(error?.localizedDescription, type: CommonAPI.LogType.Error)
                return selected
            }
            

            return selected
            
        }
        else {
            // loading ctx failed
            CommonAPI.sharedInstance.log("Failed to initialize ctx of type NSManagedObjectContext", type: CommonAPI.LogType.Error)
            return selected
            
        }
        

    }
    /*
    
    // returns the data from local if present, else gets from server and stores locally
    func getListofCountriesInBackgroundWithBlock(forceUpdateLocal: Bool, completion: (countries: [Country]!, error: NSError!) ->()) -> Void{
        var countries: [Country] = [Country]()
        
        // Step1: Get data from local
        var localQuery: PFQuery = PFQuery(className: "Countries")
        localQuery.orderByAscending("Country")
        localQuery.fromLocalDatastore()
        localQuery.findObjectsInBackgroundWithBlock { (localResults: [AnyObject]?, localError: NSError?) -> Void in
            if localError == nil {
                // if data exists localy, return them
                if localResults != nil && localResults!.count > 0 && !forceUpdateLocal{
                    println("Getting countries from local")
                    var fetchedCountries: [PFObject] = localResults as! [PFObject]
                    for fetchedCountry in fetchedCountries {
                        let country: Country = Country( id: fetchedCountry.valueForKey("objectId") as! String,
                            country: fetchedCountry.valueForKey("Country") as! String,
                            updatedAt: fetchedCountry.valueForKey("updatedAt") as! NSDate,
                            isSelectedByUser: ( fetchedCountry.valueForKey("isSelectedByUser") == nil ? false : fetchedCountry.valueForKey("isSelectedByUser") )as! Bool)
                        
                        // add it to collection
                        countries.append(country)
                    }
                    completion(countries: countries, error:localError)
                }
                // else get from server and store them locally
                else {
                    // unpin local countries
                    PFObject.unpinAllInBackground(nil, withName: "Countries", block: { (done: Bool,error: NSError?) -> Void in
                        if error == nil {
                            NSLog("Local countries unpinned")
                            
                            NSLog("Getting countries from server")
                            var query: PFQuery = PFQuery(className: "Countries")
                            query.orderByAscending("Country")
                            query.findObjectsInBackgroundWithBlock { (results: [AnyObject]?, error: NSError?) -> Void in
                                
                                if error == nil {
                                    var fetchedCountries: [PFObject] = results as! [PFObject]
                                    for fetchedCountry in fetchedCountries {
                                        let country: Country = Country( id: fetchedCountry.valueForKey("objectId") as! String,
                                            country: fetchedCountry.valueForKey("Country") as! String,
                                            updatedAt: fetchedCountry.valueForKey("updatedAt") as! NSDate,
                                            isSelectedByUser: false)
                                        
                                        // add it to collection
                                        countries.append(country)
                                    }
                                    
                                    // store them locally
                                    PFObject.pinAllInBackground(fetchedCountries, withName: "Countries", block: { (saved: Bool, error: NSError?) -> Void in
                                        if saved {
                                            NSLog("Countries saved locally, count =  \(fetchedCountries.count)")
                                        }
                                        else {
                                            NSLog("Countries couldn't be saved locally")
                                        }
                                    })
                                }
                                
                                completion(countries: countries, error:error)
                            }
                            
                            
                        }
                        else {
                            NSLog("Local countries couldn't be unpinned")
                            completion(countries: countries, error:nil)
                        }
                    })

                } // else
            }
        }

    }
    
    // returns the data from local if present, else gets from server and stores locally
    func getListofCarriersInBackgroundWithBlock(country: Country, forceUpdateLocal: Bool, completion: (carriers: [Carrier]!, error: NSError!) ->()) -> Void{
        var carriers: [Carrier] = [Carrier]()
        
        // Step1: Get data from local
        var localQuery: PFQuery = PFQuery(className: "Carriers")
        localQuery.whereKey("Country", equalTo: PFObject(withoutDataWithClassName: "Countries", objectId: country.ID))
        //localQuery.includeKey("Country.objectId")
        localQuery.orderByAscending("CarrierName")
        localQuery.fromLocalDatastore()
        localQuery.findObjectsInBackgroundWithBlock { (localResults: [AnyObject]?, localError: NSError?) -> Void in
            if localError == nil {
                // if data exists localy, return them
                if localResults != nil && localResults!.count > 0 && !forceUpdateLocal{
                    println("Getting Carriers from local")
                    var fetchedCarriers: [PFObject] = localResults as! [PFObject]
                    for fetchedCarrier in fetchedCarriers {
                        let carrier: Carrier = Carrier(id: fetchedCarrier.objectId as String!,
                            carrierName: fetchedCarrier.valueForKey("CarrierName") as! String!,
                            carrierImagePFFile: fetchedCarrier.valueForKey("CarrierImage") as! PFFile!,
                            updatedAt: fetchedCarrier.valueForKey("updatedAt")  as! NSDate!,
                            isSelectedByUser: (fetchedCarrier.valueForKey("isSelectedByUser") == nil ? false: fetchedCarrier.valueForKey("isSelectedByUser")  ) as! Bool)
                        
                        // add it to collection
                        carriers.append(carrier)
                    }
                    completion(carriers: carriers, error:localError)
                }
                    // else get from server and store them locally
                else {
                    // Unpin locals
                    PFObject.unpinAllInBackground(nil, withName: "Carriers", block: { (done: Bool, error: NSError?) -> Void in
                        if error == nil {
                            NSLog("Unpinned local Carriers")
                            
                            NSLog("Getting Carriers from server")
                            var query: PFQuery = PFQuery(className: "Carriers")
                            query.whereKey("Country", equalTo: PFObject(withoutDataWithClassName: "Countries", objectId: country.ID))
                            //query.includeKey("Country.objectId")
                            query.orderByAscending("CarrierName")
                            query.findObjectsInBackgroundWithBlock { (results: [AnyObject]?, error: NSError?) -> Void in
                                
                                if error == nil {
                                    var fetchedCarriers: [PFObject] = results as! [PFObject]
                                    for fetchedCarrier in fetchedCarriers {
                                        let carrier: Carrier = Carrier(id: fetchedCarrier.objectId as String!,
                                            carrierName: fetchedCarrier.valueForKey("CarrierName") as! String!,
                                            carrierImagePFFile: fetchedCarrier.valueForKey("CarrierImage") as! PFFile!,
                                            updatedAt: fetchedCarrier.valueForKey("updatedAt") as! NSDate!,
                                            isSelectedByUser: false)
                                        
                                        // add it to collection
                                        carriers.append(carrier)
                                    }
                                    
                                    // store them locally
                                    PFObject.pinAllInBackground(fetchedCarriers, withName: "Carriers", block: { (saved: Bool, error: NSError?) -> Void in
                                        if saved {
                                            NSLog("Carriers saved locally \(fetchedCarriers.count)")
                                        }
                                        else {
                                            NSLog("Carriers couldn't be saved locally")
                                        }
                                    })
                                }
                                
                                completion(carriers: carriers, error: error)
                            }
                            
                        }
                        else {
                            NSLog("Couldn't unpinned local Carriers")
                            completion(carriers: carriers, error: nil)
                        }
                    })

                } // else
            }
        }
        
    }
    
    func getSelectedCountry(result: (country: Country!, error: NSError!) -> ()) -> Void {
        var selectedCountry : Country? = nil
        var localQuery = PFQuery(className: "Countries")
        localQuery.fromLocalDatastore()
        localQuery.whereKey("isSelectedByUser", equalTo: true)
        localQuery.getFirstObjectInBackgroundWithBlock { (gotCountry: PFObject?, error: NSError?) -> Void in
            if error == nil {
                selectedCountry = Country(id: gotCountry!.objectId,
                    country: gotCountry!.valueForKey("Country") as! String,
                    updatedAt: gotCountry!.valueForKey("updatedAt") as! NSDate,
                    isSelectedByUser: gotCountry!.valueForKey("isSelectedByUser") as! Bool)
            }
            result(country: selectedCountry, error: error)
        }

    }
    
    func setSelectedCountry(country: Country) -> Void {
        
        var localQuery = PFQuery(className: "Countries")
        localQuery.fromLocalDatastore()
        localQuery.getObjectInBackgroundWithId(country.ID, block: { (result : PFObject?, error: NSError?) -> Void in
            if error == nil && result != nil{
               result!.setValue(country.IsSelectedByUser, forKey: "isSelectedByUser")
                result!.pinInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                    
                })
            }
        })
        
        
    }
    
    func getSelectedCarrier(result: (carrier: Carrier!, error: NSError!) -> ()) -> Void {
        var selectedCarrier : Carrier? = nil
        var localQuery = PFQuery(className: "Carriers")
        localQuery.fromLocalDatastore()
        localQuery.whereKey("isSelectedByUser", equalTo: true)
        localQuery.getFirstObjectInBackgroundWithBlock { (gotCarrier: PFObject?, error: NSError?) -> Void in
            if error == nil && gotCarrier != nil {
                // Carrier image can be null
                var carrierImage: AnyObject?  = gotCarrier!.valueForKey("CarrierImage")
                if carrierImage == nil {
                    carrierImage = PFFile(data: UIImageJPEGRepresentation(UIImage(named: "Index Cards"), 1.0))
                }
                
                
                if error == nil {
                    selectedCarrier = Carrier(id: gotCarrier!.objectId,
                        carrierName: gotCarrier!.valueForKey("CarrierName") as! String,
                        carrierImagePFFile: carrierImage as! PFFile,
                        updatedAt: gotCarrier!.valueForKey("updatedAt") as! NSDate,
                        isSelectedByUser: gotCarrier!.valueForKey("isSelectedByUser")  as! Bool)
                }
                result(carrier: selectedCarrier, error: error)
            }

        }
        
    }
    
    func setSelectedCarrier(carrier: Carrier) -> Void {
        
        var localQuery = PFQuery(className: "Carriers")
        localQuery.fromLocalDatastore()
        localQuery.getObjectInBackgroundWithId(carrier.ID, block: { (result : PFObject?, error: NSError?) -> Void in
            if error == nil && result != nil{
                NSLog("\(carrier.CarrierName) : \(carrier.IsSelectedByUser)")
                result!.setValue(carrier.IsSelectedByUser, forKey: "isSelectedByUser")
                result!.pinInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                    // It also means, the user has finished setting preferences
                    CommonAPI.sharedInstance.PreferencesSetForFirstTime = true
                })
            }
        })
    }
    
    func clearSelectedCarrier() -> Void {
        var localQuery = PFQuery(className: "Carriers")
        localQuery.fromLocalDatastore()
        localQuery.whereKey("isSelectedByUser", equalTo: true)
        localQuery.getFirstObjectInBackgroundWithBlock { (result: PFObject?, error: NSError?) -> Void in
            if error == nil && result != nil {
                result!.setValue(false, forKey: "isSelectedByUser")
                result!.pinInBackgroundWithBlock({ (success: Bool, error: NSError?) -> Void in
                    
                })
            }
        }
    }
    
    // Gets the carrier codes for a carrier selected
    func getCarrierCodes(forceUpdateLocal : Bool, result: (codes: [CarrierCode]!, error: NSError!) -> ()) -> Void {
        var carrierCodesList : [CarrierCode] = []
        
        var allCodes : [CarrierCode] = getAllCarrierCodes()
        if allCodes.count > 0 {
            for code in allCodes {
                println(code)
            }
        }
        
        // get the selected carrier
        getSelectedCarrier { (carrier, error) -> () in
            if error == nil && carrier != nil {
                // Find locally
                var localQuery = PFQuery(className: "Codes")
                localQuery.fromLocalDatastore()
                localQuery.whereKey("Carrier", equalTo: PFObject(withoutDataWithClassName: "Carriers", objectId: carrier.ID))
                localQuery.findObjectsInBackgroundWithBlock({ (codes: [AnyObject]?, error: NSError?) -> Void in
                    if !forceUpdateLocal && error == nil && codes != nil && codes!.count > 0{
                        // found
                        NSLog("getCarrierCodes: getting from local")
                        for code in codes! {
                            let codePFObj = code as! PFObject
                            var carrCode : CarrierCode = CarrierCode(id: codePFObj.valueForKey("objectId") as! String,
                                code: codePFObj.valueForKey("Code") as! String,
                                description: codePFObj.valueForKey("Description") as! String,
                                updatedAt: codePFObj.valueForKey("updatedAt") as! NSDate,
                                isFavouriteByUser: (codePFObj.valueForKey("isFavouriteByUser") == nil ? false : codePFObj.valueForKey("isFavouriteByUser")) as! Bool)
                            
                            // add it to carrierCodesList
                            carrierCodesList.append(carrCode)
                         
                            
                        }
                        
                        // return
                        result(codes: carrierCodesList, error: nil)
                    }
                    else {
            
                        // Unpin local
                        PFObject.unpinAllObjectsInBackgroundWithName("Carriers", block: { (unpinned: Bool, error: NSError?) -> Void in
                            if error == nil {
                                NSLog("Local carrier codes unpinned")
                                // fetch from server
                                var serverQuery = PFQuery(className: "Codes")
                                serverQuery.whereKey("Carrier", equalTo: PFObject(withoutDataWithClassName: "Carriers", objectId: carrier.ID))
                                serverQuery.findObjectsInBackgroundWithBlock({ (codes: [AnyObject]?, error: NSError?) -> Void in
                                    if error == nil && codes != nil && codes!.count > 0 {
                                        NSLog("getCarrierCodes: getting from server")
                                        for code in codes! {
                                            let codePFObj = code as! PFObject
                                            var carrCode : CarrierCode = CarrierCode(id: codePFObj.valueForKey("objectId") as! String,
                                                code: codePFObj.valueForKey("Code") as! String,
                                                description: codePFObj.valueForKey("Description") as! String,
                                                updatedAt: codePFObj.valueForKey("updatedAt") as! NSDate,
                                                isFavouriteByUser: false)
                                            
                                            // add it to carrierCodesList
                                            carrierCodesList.append(carrCode)
                                            
                                            self.saveCarrierCode(carrCode)
                                        }
                                        
                                        // store them locally
                                        PFObject.pinAllInBackground(codes, withName: "Codes", block: { (saved: Bool, error: NSError?) -> Void in
                                            if error == nil && saved {
                                                NSLog("getCarrierCodes: Saved codes to local, count = \(codes!.count)")
                                            }
                                        })
                                        
                                    }
                                    result(codes: carrierCodesList, error: nil)
                                })
                                
                            }
                            else {
                                NSLog("Local carrier codes couldn't be unpinned")
                            }
                        })
    
                        
                    } // else
                })
            }
            else {
                result(codes: carrierCodesList, error: nil)
            }
        }
    }
    
    
    func setCodeFavourite(id: String, state: Bool, result: (success: Bool) ->()) -> Void {
        
        var localQuery = PFQuery(className: "Codes")
        localQuery.fromLocalDatastore()
        localQuery.getObjectInBackgroundWithId(id, block: { (code: PFObject?, error: NSError?) -> Void in
            if error == nil && code != nil {
                code!.setValue(state, forKey: "isFavouriteByUser")
                code?.pinInBackgroundWithBlock({ (saved: Bool, error: NSError?) -> Void in
                    result(success: saved)
                })
                
            } else {
                result(success: false)
            }
        })
    }
    
    func getFavouriteCodes(id: String, result: (codes: [CarrierCode]!, error: NSError!) -> ()) -> Void {
        var carrierCodesList : [CarrierCode] = []
        
        var localQuery = PFQuery(className: "Codes")
        localQuery.fromLocalDatastore()
        localQuery.whereKey("Carrier", equalTo: PFObject(withoutDataWithClassName: "Carriers", objectId: id))
        localQuery.whereKey("isFavouriteByUser", equalTo: true)
        localQuery.findObjectsInBackgroundWithBlock { (codes: [AnyObject]?, error: NSError?) -> Void in
            if error == nil && codes != nil && codes!.count > 0 {
                NSLog("getFavouriteCodes")
                for code in codes! {
                    let codePFObj = code as! PFObject
                    var carrCode : CarrierCode = CarrierCode(id: codePFObj.valueForKey("objectId") as! String,
                        code: codePFObj.valueForKey("Code") as! String,
                        description: codePFObj.valueForKey("Description") as! String,
                        updatedAt: codePFObj.valueForKey("updatedAt") as! NSDate,
                        isFavouriteByUser: true)
                    
                    // add it to carrierCodesList
                    carrierCodesList.append(carrCode)
                }
            }
            result(codes: carrierCodesList, error: nil)
        }
    }
    
    */

    func loadParseConfig(result: (config: PFConfig!, error: NSError!) -> ()) -> Void{
        PFConfig.getConfigInBackgroundWithBlock { (config: PFConfig?, error: NSError?) -> Void in
            result(config: config, error: error)
        }
    }

}
