//
//  SettingsTableViewController.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/14/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    var boxView = UIView()
    var configData : PFConfig!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        var nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.BlackTranslucent
        nav?.barTintColor = CommonAPI.sharedInstance.hexStringToUIColor("#FF9500")
        nav?.tintColor = UIColor.whiteColor()
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        
        // Load Config
        DataAccessAPI.SharedInstance.loadParseConfig { (config, error) -> () in
            if error == nil {
                NSLog("Fetched parse config")
                self.configData = config
            }
            else {
                NSLog("Error fetching parse config")
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 && indexPath.row == 1 {
            NSLog("Going to update the data")
            
            showUpdatingView()
            
            // get selected country
            var selectedCountry = DataAccessAPI.SharedInstance.getSelectedCountry()
            
            // get selected carrier
            var selectedCarrier: Carrier?
            
            if let selectedCountryObj = selectedCountry {
                selectedCarrier = DataAccessAPI.SharedInstance.getSelectedCarrier(selectedCountryObj)
            }
        
            // force update countries
            DataAccessAPI.SharedInstance.getCountries(forceUpdate: true, completion: { (countries, error) -> () in
                self.removeUpdatingView()
            })
            
        }
        
        if indexPath.section == 1 && indexPath.row == 0 {
            var url: NSURL = NSURL(string: self.configData["AppShareURL"] as! String)!
            let shareText = self.configData["AppShareText"] as! String
            
            var firstActivityItem : String  = ""
            if shareText.isEmpty {
                firstActivityItem = shareText
            }
            else {
                firstActivityItem = "I use Balance Buddy to check my balance. Download now!"
            }
            
            
            let secondActivityItem : NSURL = url
            
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [firstActivityItem, secondActivityItem], applicationActivities: nil)
            
            
            activityViewController.excludedActivityTypes = [
                UIActivityTypePostToWeibo,
                UIActivityTypePrint,
                UIActivityTypeAssignToContact,
                UIActivityTypeSaveToCameraRoll,
                UIActivityTypeAddToReadingList,
                UIActivityTypePostToFlickr,
                UIActivityTypePostToVimeo,
                UIActivityTypePostToTencentWeibo
            ]
            
            self.presentViewController(activityViewController, animated: true, completion: nil)
        }
        
        if indexPath.section == 1 && indexPath.row == 1 {
            
            var url: NSURL = NSURL(string: self.configData["AppStoreURL"] as! String)!
            NSLog("Will open app store \(url)")
            UIApplication.sharedApplication().openURL(url)
        }
        
        if indexPath.section == 2 && indexPath.row == 0 {
            var url: NSURL = NSURL(string: self.configData["FeedbackURL"] as! String )!
            NSLog("Will got to feedback \(url)")
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func showUpdatingView() {
        // You only need to adjust this frame to move it anywhere you want
        boxView = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25, width: 180, height: 50))
        boxView.backgroundColor = UIColor.blueColor()
        boxView.alpha = 0.8
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        var activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        var textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.whiteColor()
        textLabel.text = "Updating"
        
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)
    }
    
    func removeUpdatingView() {
        boxView.removeFromSuperview()
    }
    // MARK: - Table view data source
/*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 0
    }
*/
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SettingsToPreferencesSegue" {
            CommonAPI.sharedInstance.PreferenceSourceInitiator = "MainView"
        }
    }


}
