//
//  CommonAPI.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/7/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit

class CommonAPI: NSObject {
   
    var prefSourceInitiator: String = ""
    class var sharedInstance : CommonAPI {
        struct Singleton {
            static let instance = CommonAPI()
        }
        
        return Singleton.instance
    }
    
    func isOnline() -> Bool {
        return true
    }
    
    var PreferencesSetForFirstTime : Bool{
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey("PreferencesSetForFirstTime")
        }
        set (state) {
            NSUserDefaults.standardUserDefaults().setBool(state, forKey: "PreferencesSetForFirstTime")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var PreferenceSourceInitiator: String {
        get {
            return prefSourceInitiator
        }
        set(value){
            prefSourceInitiator = value
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(advance(cString.startIndex, 1))
        }
        
        if (count(cString) != 6) {
            return UIColor.grayColor()
        }
        
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    enum LogType : String, Printable{
        case Information = "Information"
        case Warning = "Warning"
        case Error = "Error"
        
        var description: String {
            return self.rawValue
        }
    }
    
    func log(message: String?, type: LogType) -> Void {
        NSLog("[\(type.description)] \(message)")
    }
}
