//
//  LaunchViewController.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/6/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
        // check if first time preferences are set
        if CommonAPI.sharedInstance.PreferencesSetForFirstTime {
            performSegueWithIdentifier("LaunchToMainSegue", sender: self)
            CommonAPI.sharedInstance.PreferenceSourceInitiator = "MainView"
        }
        else {
            performSegueWithIdentifier("SetPreferencesSegue", sender: self)
            CommonAPI.sharedInstance.PreferenceSourceInitiator = "LaunchView"
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "LaunchToPreferencesNavSegue" {
            CommonAPI.sharedInstance.PreferenceSourceInitiator = "LaunchView"
        }
    }


}
