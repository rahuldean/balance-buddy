//
//  CountryPreferenceTableViewController.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/7/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit

class CountryPreferenceTableViewController: UITableViewController {

    var countriesList: [Country] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.loadData()
        
    }

    override func viewDidAppear(animated: Bool) {
        self.loadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell

        let currentCountry = self.countriesList[indexPath.row]
        cell.textLabel?.text = currentCountry.countryName
        
        if currentCountry.isSelected.boolValue {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
            tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
            
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        var currentCountry = self.countriesList[indexPath.row]

        var selectedCarrier: Carrier?
        
        cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
        
        // get selected country
        var selectedCountry = DataAccessAPI.SharedInstance.getSelectedCountry()
        
        if selectedCountry != nil {
            // get selected carrier
            selectedCarrier = DataAccessAPI.SharedInstance.getSelectedCarrier(selectedCountry!)
            selectedCarrier?.isSelected = 0
        }
        
        // update the selection
        currentCountry.isSelected = 1
        DataAccessAPI.SharedInstance.updateCountry(currentCountry)
        
        // Fix: When user selects different country, clear the carrier
        if selectedCarrier != nil {
            DataAccessAPI.SharedInstance.updateCarrier(selectedCarrier!)
        }
        
        // Get new carrirer data
        DataAccessAPI.SharedInstance.getCarriers(currentCountry, forceUpdate: true) { (carriers, error) -> () in
            
        }
        
        
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        
        var currentCountry = self.countriesList[indexPath.row]
        
        cell?.accessoryType = UITableViewCellAccessoryType.None
        
        // update the selection
        currentCountry.isSelected = 0
        DataAccessAPI.SharedInstance.updateCountry(currentCountry)
        

    }
    private func loadData() -> Void{
        // Get the countries data
        DataAccessAPI.SharedInstance.getCountries(forceUpdate: false) { (countries, error) -> () in
            if error == nil {
                self.countriesList = countries!
                self.tableView.reloadData()
            }
        }
    }

}
