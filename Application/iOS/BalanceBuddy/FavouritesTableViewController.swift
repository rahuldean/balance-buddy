//
//  FavouritesTableViewController.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/15/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit
import Batch

class FavouritesTableViewController: UITableViewController {

    var codesData: [Code] = []
    var selectedCarrier: Carrier!
    var selectedCode : Code!
    let adsAvailable = BatchAds.hasAdForPlacement(BatchPlacementDefault)
    
    override func viewDidLoad() {
        super.viewDidLoad()

 
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        CommonAPI.sharedInstance.PreferenceSourceInitiator = "MainView"
        
        var nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.BlackTranslucent
        nav?.barTintColor = CommonAPI.sharedInstance.hexStringToUIColor("#FF2851")
        nav?.tintColor = UIColor.whiteColor()
        nav?.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
    }

    override func viewDidAppear(animated: Bool) {

        // get selected country
        var selectedCountry = DataAccessAPI.SharedInstance.getSelectedCountry()
        
        // get selected carrier
        var selectedCarrier: Carrier?
        
        if let selectedCountryObj = selectedCountry {
            selectedCarrier = DataAccessAPI.SharedInstance.getSelectedCarrier(selectedCountryObj)
        }
        
        if selectedCarrier != nil {
            codesData = DataAccessAPI.SharedInstance.getFavouriteCodes(selectedCarrier!)
            self.selectedCarrier = selectedCarrier
            self.tableView.reloadData()
        }
        else {
            self.codesData = []
            self.tableView.reloadData()
        }
        
        if(adsAvailable) {
            showAd()
        }
        else {
            NSLog("Ads not available")
        }
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return codesData.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell

        let currentCode = codesData[indexPath.row]
        cell.textLabel?.text = currentCode.codeDescription
        cell.detailTextLabel?.text = currentCode.code
        
        return cell
    }


    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let currentCode = codesData[indexPath.row]
        
        var url:NSURL = NSURL(string: "tel://" + currentCode.code)!
        UIApplication.sharedApplication().openURL(url)
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        NSLog("tapped")
        self.selectedCode = codesData[indexPath.row]
        performSegueWithIdentifier("CarrierCodeInfoSegue", sender: self)
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "CarrierCodeInfoSegue" {
            var destination = segue.destinationViewController as! CarrierCodeInfoTableViewController
            
            destination.codeId = self.selectedCode.objectId
            destination.carrierName = self.selectedCarrier.carrierName
            destination.carrierCode = self.selectedCode.code
            destination.carrierDescription = self.selectedCode.codeDescription
            
            destination.isFavouriteByUser = self.selectedCode.isFavouriteByUser.boolValue
        }
    }
    
    func showAd()
    {
        if !BatchAds.displayAdForPlacement(BatchPlacementDefault)
        {
            // No ad to display or ad cannot be displayed.
            NSLog("No ad to display or ad cannot be displayed")
        }
        else {
            NSLog("Ad displayed")
        }
    }

}
