//
//  PreferencesTableViewController.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/7/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit

class PreferencesTableViewController: UITableViewController {

    @IBOutlet weak var countryTVCell: UITableViewCell!
    @IBOutlet weak var carrierTVCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        var segueInitiator: String = CommonAPI.sharedInstance.PreferenceSourceInitiator
        
        // Enable/ disable
        switch segueInitiator{
        case "LaunchView" :
            NSLog("From launch view")
        case "MainView" :
            NSLog("From main view")
            self.navigationItem.rightBarButtonItem = nil
        default:
            NSLog("")
            self.navigationItem.rightBarButtonItem = nil
        }
    }

    override func viewDidAppear(animated: Bool) {
        
        // get selected country
        var selectedCountry = DataAccessAPI.SharedInstance.getSelectedCountry()
        var selectedCarrier: Carrier?
        
        if selectedCountry != nil {
            self.countryTVCell.detailTextLabel?.text = selectedCountry?.countryName
            
            // get selected carrier
            selectedCarrier = DataAccessAPI.SharedInstance.getSelectedCarrier(selectedCountry!)
        }
        else {
            self.countryTVCell.detailTextLabel?.text = "Not selected"
        }
        
        if selectedCarrier != nil {
            self.carrierTVCell.detailTextLabel?.text = selectedCarrier?.carrierName
        }
        else {
            self.carrierTVCell.detailTextLabel?.text = "Not selected"
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 0
    }
    */
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
