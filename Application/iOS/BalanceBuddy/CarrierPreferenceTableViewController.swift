//
//  CarrierPreferenceTableViewController.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 3/7/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import UIKit

class CarrierPreferenceTableViewController: UITableViewController {

    @IBOutlet weak var messageLabel: UILabel!
    
    var selectedCountry: Country!
    
    var carriersList: [Carrier] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // get selected country
        var selectedCountry = DataAccessAPI.SharedInstance.getSelectedCountry()
        if selectedCountry != nil{
            self.messageLabel.hidden = true
            self.selectedCountry = selectedCountry
            self.loadData()
        }
        else {
            self.messageLabel.hidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carriersList.count
    }
    
    private func loadData() -> Void {
        if self.selectedCountry != nil {
            
            DataAccessAPI.SharedInstance.getCarriers(self.selectedCountry, forceUpdate: true, completion: { (carriers, error) -> () in
                if error == nil {
                    self.carriersList = carriers!
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        var currentCarrier = self.carriersList[indexPath.row]
        cell.textLabel?.text = currentCarrier.carrierName
       
        // set the check mark
        if currentCarrier.isSelected == true  {
            cell.accessoryType = .Checkmark
            tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
        }
        else {
            cell.accessoryType = .None
        }
        
        // Set the image
        if currentCarrier.carrierImageURL != nil {
            let url = NSURL(string: currentCarrier.carrierImageURL!)
            if let data = NSData(contentsOfURL: url!) {
                cell.imageView?.image = UIImage(data: data)
            }
            else {
                cell.imageView?.image = UIImage(named: "Index Cards")
            }
            cell.imageView?.sizeToFit()
            self.tableView.reloadData()
            
        }
        else {
            cell.imageView?.image = UIImage(named: "Index Cards")
        }
        
        return cell
    }


    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .Checkmark
        // check if preference is set
        if !CommonAPI.sharedInstance.PreferencesSetForFirstTime {
            CommonAPI.sharedInstance.PreferencesSetForFirstTime = true
        }
        
        var currentCarrier: Carrier = self.carriersList[indexPath.row]
        
        // update the selection
        currentCarrier.isSelected = 1
        DataAccessAPI.SharedInstance.updateCarrier(currentCarrier)
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = .None
        
        var currentCarrier: Carrier = self.carriersList[indexPath.row]
        
        // update the selection
        currentCarrier.isSelected = 0
        DataAccessAPI.SharedInstance.updateCarrier(currentCarrier)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
