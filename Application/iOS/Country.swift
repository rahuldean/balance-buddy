//
//  Country.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 4/19/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import Foundation
import CoreData

class Country: NSManagedObject {

    @NSManaged var objectId: String
    @NSManaged var countryName: String
    @NSManaged var updatedAt: NSDate
    @NSManaged var isSelected: NSNumber
    @NSManaged var carrierRel: NSSet

}
