//
//  Code.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 4/19/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import Foundation
import CoreData

class Code: NSManagedObject {

    @NSManaged var code: String
    @NSManaged var codeDescription: String
    @NSManaged var isFavouriteByUser: NSNumber
    @NSManaged var objectId: String
    @NSManaged var updatedAt: NSDate
    @NSManaged var carrierRel: Carrier

}
