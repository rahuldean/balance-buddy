//
//  Carrier.swift
//  BalanceBuddy
//
//  Created by Rahul Reddy on 4/19/15.
//  Copyright (c) 2015 Saptana. All rights reserved.
//

import Foundation
import CoreData

class Carrier: NSManagedObject {

    @NSManaged var objectId: String
    @NSManaged var carrierName: String
    @NSManaged var carrierImageURL: String?
    @NSManaged var updatedAt: NSDate
    @NSManaged var isSelected: NSNumber
    @NSManaged var codeRel: NSSet
    @NSManaged var countryRel: Country

}
