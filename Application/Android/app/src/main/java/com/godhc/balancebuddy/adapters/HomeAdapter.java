package com.godhc.balancebuddy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.godhc.balancebuddy.R;
import com.godhc.balancebuddy.models.Carrier;
import com.godhc.balancebuddy.network.VolleySingleton;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private Context context;
    private List<Carrier> carrierList;

    private ExploreClickListener exploreClickListener;

    public HomeAdapter(Context context, List<Carrier> carrierList) {
        super();
        this.context = context;
        this.carrierList = carrierList;
    }

    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.home_recycler_view_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HomeAdapter.ViewHolder viewHolder, final int i) {

        Carrier currentCarrier  = this.carrierList.get(i);

        viewHolder.titleTextView.setText(currentCarrier.getCarrierName());

        ImageLoader imageLoader = VolleySingleton.getInstance(this.context).getImageLoader();
        String url = "http://lorempixel.com/64/64/city/"+ i %10 + "/";

        if (currentCarrier.getCarrierImageExternalUrl() != null){
            url = currentCarrier.getCarrierImageExternalUrl();
        }
        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                viewHolder.avatarCircleImageView.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        viewHolder.exploreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exploreClickListener.onExploreClick(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (this.carrierList != null){
            return this.carrierList.size();
        }
        else
        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        CircleImageView avatarCircleImageView;
        Button exploreButton;

        public ViewHolder(final View itemView) {
            super(itemView);

            titleTextView = (TextView) itemView.findViewById(R.id.home_rv_row_title);
            avatarCircleImageView = (CircleImageView) itemView.findViewById(R.id.avatar_image);
            exploreButton = (Button) itemView.findViewById(R.id.home_recycler_view_explore);
        }
    }

    public void setExploreClickListener(ExploreClickListener exploreClickListener){
        this.exploreClickListener = exploreClickListener;
    }

    public interface ExploreClickListener {
        void onExploreClick(int position);
    }

    public void refresh(List<Carrier> carrierList){
        this.carrierList = carrierList;
        notifyDataSetChanged();
    }
}
