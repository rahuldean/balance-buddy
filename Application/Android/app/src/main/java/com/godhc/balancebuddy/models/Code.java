package com.godhc.balancebuddy.models;

import org.parceler.Parcel;

import java.util.Date;

@Parcel
public class Code {
    public Code() {
        super();
    }

    public Code(String objectId, String code, String description, Date updatedAt) {
        this.objectId = objectId;
        this.code = code;
        this.description = description;
        this.updatedAt = updatedAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    String objectId;
    String code;
    String description;
    Date updatedAt;
}
