package com.godhc.balancebuddy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.godhc.balancebuddy.activities.CarrierDetailsActivity;
import com.godhc.balancebuddy.activities.SettingsActivity;
import com.godhc.balancebuddy.adapters.HomeAdapter;
import com.godhc.balancebuddy.models.Carrier;
import com.godhc.balancebuddy.network.VolleySingleton;
import com.godhc.balancebuddy.utils.Keys;
import com.orhanobut.logger.Logger;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import static com.godhc.balancebuddy.utils.Constants.ParseClasses.*;
import static com.godhc.balancebuddy.utils.Constants.ParseColumns.*;
import static com.godhc.balancebuddy.utils.Constants.SharedPreferencesKeys.*;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity implements HomeAdapter.ExploreClickListener {
    private HomeAdapter homeAdapter;
    private List<Carrier> carriers = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String appName = getResources().getString(R.string.app_name);
        String default_header_url = getResources().getString(R.string.DEFAULT_URL_HOME_HEADER);
        final int headerOverlayColor = getResources().getColor(R.color.headerOverlay);

        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.home_collapsingToolbarLayout);


        if (collapsingToolbarLayout != null && appName != null && !appName.isEmpty()) {
            collapsingToolbarLayout.setTitle(appName);
        }

        RecyclerView homeRecyclerView = (RecyclerView) findViewById(R.id.home_recyclerView);
        homeRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, 1));
        homeRecyclerView.setHasFixedSize(false);
        homeAdapter = new HomeAdapter(this, carriers);
        homeAdapter.setExploreClickListener(this);
        homeRecyclerView.setAdapter(homeAdapter);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.home_swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(refreshListener());
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        boolean refresh = sharedPreferences.getBoolean(REFRESH_CARRIERS, false);

        if (refresh) {
            loadData();

            editor.putBoolean(REFRESH_CARRIERS, false);
            editor.apply();
            Logger.d("refresh_carriers set to false");
        } else {
            // See if data is available in saved instance
            if (savedInstanceState != null) {
                carriers.clear();
                carriers = Parcels.unwrap(savedInstanceState.getParcelable(Keys.PARCEL_CARRIERS_KEY));
                homeAdapter.refresh(carriers);
                Logger.d("Loaded carriers data (%d) from saved instance", carriers.size());

            } else
                this.loadData();
        }

        // Load header image
        VolleySingleton volleySingleton = VolleySingleton.getInstance(this);
        ImageLoader imageLoader = volleySingleton.getImageLoader();

        final ImageView headerImageView = (ImageView) findViewById(R.id.header);
        imageLoader.get(default_header_url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                headerImageView.setImageBitmap(response.getBitmap());
                headerImageView.setBackgroundColor(headerOverlayColor);
                headerImageView.setAlpha(180);
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private SwipeRefreshLayout.OnRefreshListener refreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        };
    }

    private void loadData() {
        Logger.d("Loading carriers data from server");
        swipeRefreshLayout.setRefreshing(true);
        Logger.d("Carriers list cleared");

        final View parentView = findViewById(R.id.home_coordinatorLayout);

        String preferredCountry = PreferenceManager.getDefaultSharedPreferences(this).getString("general_preferred_country", "-1");
        Logger.d("Preferred country = %s ", preferredCountry);

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Carriers");
        if (preferredCountry != null && !preferredCountry.contentEquals("-1")) {
            ParseObject countryObject = ParseObject.createWithoutData(COUNTRIES, preferredCountry);
            query.whereEqualTo(COUNTRY, countryObject);
        }
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
        query.addAscendingOrder(CARRIER_NAME);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> carriersList, ParseException e) {
                if (e == null) {
                    // Since this block may be entered twice (cache -> Network)
                    carriers.clear();
                    for (int i = 0; i < carriersList.size(); i++) {
                        Carrier carrier = new Carrier(carriersList.get(i).getObjectId(),
                                carriersList.get(i).getString("CarrierName"),
                                carriersList.get(i).getString("CarrierImageExternal"));

                        carriers.add(carrier);
                    }

                    homeAdapter.refresh(carriers);
                    Logger.d("%d Carriers fetched from server", carriers.size());

                    showSnackBarMessage(parentView, "Information is now up to date");
                    swipeRefreshLayout.setRefreshing(false);

                } else {
                    Logger.e("Failed to get carriers from server", e.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                    showSnackBarMessage(parentView, "Something went wrong!");
                }
            }
        });
    }

    @Override
    public void onExploreClick(int position) {
        Logger.d("Clicked explore at position %d", position);

        Intent intent = new Intent(this, CarrierDetailsActivity.class);
        intent.putExtra(Keys.PARCEL_CARRIERS_KEY, Parcels.wrap(this.carriers.get(position)));

        startActivity(intent);
    }

    private void showSnackBarMessage(View parentView, String message) {
        Snackbar.make(parentView, "" + message, Snackbar.LENGTH_LONG)
                .setAction("DISMISS", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                })
                .show();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(Keys.PARCEL_CARRIERS_KEY, Parcels.wrap(carriers));
    }
}
