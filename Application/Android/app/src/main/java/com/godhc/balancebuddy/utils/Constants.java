package com.godhc.balancebuddy.utils;

public final class Constants {

    /* Parse Classes*/
    public final class ParseClasses {
        public static final String COUNTRIES = "Countries";
    }

    /* Parse Column names */
    public final class ParseColumns {
        public static final String COUNTRY = "Country";
        public static final String  CARRIER_NAME = "CarrierName";
    }

    public final class ApplicationErrorMessages {
        public static final String FAILED_TO_GET_COUNTRY_DATA = "Failed to get country data from server or cache";
    }

    public final class Flurry {

    }

    public final class SharedPreferencesKeys {
        public static final String REFRESH_CARRIERS = "REFRESH_CARRIERS";
    }

}
