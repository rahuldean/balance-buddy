package com.godhc.balancebuddy;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.parse.Parse;
import com.parse.ParseCrashReporting;

public class Application extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        Parse.initialize(this, "fIDWyhCQMGIU8bq2FyALutgC1vu0Lq4uxrC6Uark", "15xcUtXSJV2v3bLd6k22LeSOoW1NEFAgERRVphaA");

        // configure Flurry
        FlurryAgent.setLogEnabled(false);
        //FlurryAgent.setLogLevel(Log.VERBOSE);

        // init Flurry
        FlurryAgent.init(this, "J892TVRWC8CYVTNKTQJ4");
    }

}
