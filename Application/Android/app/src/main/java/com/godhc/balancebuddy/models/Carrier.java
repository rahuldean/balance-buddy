package com.godhc.balancebuddy.models;

import org.parceler.Parcel;

@Parcel
public class Carrier {

    public Carrier() {
        super();
    }

    public Carrier(String objectId, String carrierName, String carrierImageExternalUrl) {
        this.objectId = objectId;
        this.carrierName = carrierName;
        this.carrierImageExternalUrl = carrierImageExternalUrl;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCarrierImageExternalUrl() {
        return carrierImageExternalUrl;
    }

    public void setCarrierImageExternalUrl(String carrierImageExternalUrl) {
        this.carrierImageExternalUrl = carrierImageExternalUrl;
    }

    String objectId;
    String carrierName;
    String carrierImageExternalUrl;
}
