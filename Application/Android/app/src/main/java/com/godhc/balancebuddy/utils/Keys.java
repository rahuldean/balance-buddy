package com.godhc.balancebuddy.utils;

public interface Keys {
    String PARCEL_CARRIERS_KEY = "PARCEL_CARRIERS_KEY";
    String PARCEL_CARRIER_CODES_KEY = "PARCEL_CARRIER_CODES_KEY";
}
