package com.godhc.balancebuddy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.godhc.balancebuddy.R;
import com.godhc.balancebuddy.models.Code;

import java.util.ArrayList;
import java.util.List;

public class CarrierDetailsAdapter extends RecyclerView.Adapter<CarrierDetailsAdapter.ViewHolder> implements View.OnClickListener{

    private Context context;
    private List<Code> codeList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public CarrierDetailsAdapter(Context context) {
        super();
        this.context =  context;
    }

    public void setData(List<Code> codeList){
        this.codeList = codeList;
        notifyDataSetChanged();
    }

    public void setItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }

    @Override
    public CarrierDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context).inflate(R.layout.carrier_details_row, parent, false);
        view.setOnClickListener(this);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CarrierDetailsAdapter.ViewHolder holder, final int position) {
        Code currentCode = this.codeList.get(position);

        if (currentCode != null){
            if (!currentCode.getCode().isEmpty())
                holder.code.setText(currentCode.getCode());

            if (!currentCode.getDescription().isEmpty()){
                String description = currentCode.getDescription();
                holder.description.setText(description);

                String letter = description.substring(0, 1);
                int color = ColorGenerator.MATERIAL.getColor(description);

                TextDrawable drawable = TextDrawable.builder()
                        .beginConfig()
                        .toUpperCase()
                        .endConfig()
                        .buildRoundRect(letter, color, 10);

                holder.image.setImageDrawable(drawable);

            }
        }
    }

    @Override
    public int getItemCount() {
        return this.codeList.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{
        TextView code;
        TextView description;
        ImageView image;
        public ViewHolder(View itemView) {
            super(itemView);

            code = (TextView) itemView.findViewById(R.id.carrier_details_code);
            description = (TextView) itemView.findViewById(R.id.carrier_details_description);
            image = (ImageView) itemView.findViewById(R.id.carrier_details_image);
        }
    }

    @Override
    public void onClick(View view) {
        if (itemClickListener != null)
            itemClickListener.onItemClickListener(view);
    }

    public interface ItemClickListener {
        void onItemClickListener(View view);
    }
}
