package com.godhc.balancebuddy.models;

public class Country {
    public Country() {
        super();
    }

    public Country(String objectId, String country) {
        this.objectId = objectId;
        Country = country;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    String objectId;
    String Country;
}
