package com.godhc.balancebuddy.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.flurry.android.FlurryAgent;
import com.flurry.android.ads.FlurryAdErrorType;
import com.flurry.android.ads.FlurryAdInterstitial;
import com.flurry.android.ads.FlurryAdInterstitialListener;
import com.godhc.balancebuddy.R;
import com.godhc.balancebuddy.adapters.CarrierDetailsAdapter;
import com.godhc.balancebuddy.models.Carrier;
import com.godhc.balancebuddy.models.Code;
import com.godhc.balancebuddy.network.VolleySingleton;
import com.godhc.balancebuddy.utils.Keys;
import com.orhanobut.logger.Logger;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class CarrierDetailsActivity extends AppCompatActivity implements CarrierDetailsAdapter.ItemClickListener {

    private List<Code> codeList = new ArrayList<>();
    CarrierDetailsAdapter carrierDetailsAdapter;
    RecyclerView recyclerView;

    private String mAdSpaceName = "Ad1";
    private FlurryAdInterstitial mFlurryAdInterstitial = null;

    @Override
    public void onStart() {
        super.onStart();
        //for ICE Cream Sandwich and higher you can skip the FlurryAgent.onStartSession;
        FlurryAgent.onStartSession(this, "J892TVRWC8CYVTNKTQJ4");
        mFlurryAdInterstitial = new FlurryAdInterstitial(this, mAdSpaceName);
        mFlurryAdInterstitial.setListener(interstitialAdListener);
        mFlurryAdInterstitial.fetchAd();
    }

    @Override
    public void onStop() {
        super.onStop();
        //for ICE Cream Sandwich and higher you can skip the FlurryAgent.onEndSession;

        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFlurryAdInterstitial.destroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrier_details);

        mFlurryAdInterstitial = new FlurryAdInterstitial(this, mAdSpaceName);
        mFlurryAdInterstitial.setListener(interstitialAdListener);

        Toolbar toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.home_collapsingToolbarLayout);

        // Get the data from intent
        Carrier carrierSelected = Parcels.unwrap(getIntent().getExtras().getParcelable(Keys.PARCEL_CARRIERS_KEY));

        if (carrierSelected != null) {
            Logger.d("Got data from parcel");
            if (collapsingToolbarLayout != null) {
                collapsingToolbarLayout.setTitle(carrierSelected.getCarrierName());
            }

            VolleySingleton volleySingleton = VolleySingleton.getInstance(this);
            ImageLoader imageLoader = volleySingleton.getImageLoader();

            String url = "http://lorempixel.com/250/250/city/";

            if (carrierSelected.getCarrierImageExternalUrl() != null && !carrierSelected.getCarrierImageExternalUrl().isEmpty())
                url = carrierSelected.getCarrierImageExternalUrl();

            final ImageView imageView = (ImageView) findViewById(R.id.header);
            imageLoader.get(url, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imageView.setImageBitmap(response.getBitmap());
                    imageView.setBackgroundColor(Color.parseColor("#11000000"));
                    imageView.setAlpha(180);
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    Logger.e("Error while getting carrier image", error);
                }
            });

            recyclerView = (RecyclerView) findViewById(R.id.carrier_details_recycler_view);
            carrierDetailsAdapter = new CarrierDetailsAdapter(this);
            carrierDetailsAdapter.setData(codeList);
            carrierDetailsAdapter.setItemClickListener(this);

            recyclerView.setAdapter(carrierDetailsAdapter);
            recyclerView.setHasFixedSize(false);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            if (savedInstanceState != null) {
                this.codeList = Parcels.unwrap(savedInstanceState.getParcelable(Keys.PARCEL_CARRIER_CODES_KEY));
                carrierDetailsAdapter.setData(this.codeList);
                Logger.d("Loaded codes(%d) from saved instance for %s", codeList.size(), carrierSelected.getObjectId());
            } else
                loadData(carrierSelected.getObjectId());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_carrier_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClickListener(View view) {
        int position = recyclerView.getChildAdapterPosition(view);
        Code selectedCode = this.codeList.get(position);
        if (selectedCode != null) {
            String ussdCode = Uri.encode(selectedCode.getCode());
            startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + ussdCode)));

        }
        showAd();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(Keys.PARCEL_CARRIER_CODES_KEY, Parcels.wrap(this.codeList));
    }

    private void loadData(final String carrierObjectId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Codes");
        ParseObject carrierObject = ParseObject.createWithoutData("Carriers", carrierObjectId);
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
        query.whereEqualTo("Carrier", carrierObject);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> carrierCodeslist, ParseException e) {
                if (e == null) {
                    // this block can be entered more than once, one for cache response and then for network
                    codeList.clear();
                    for (int i = 0; i < carrierCodeslist.size(); i++) {
                        ParseObject currentCarrierCode = carrierCodeslist.get(i);
                        Code currentCode = new Code(currentCarrierCode.getObjectId(),
                                currentCarrierCode.getString("Code"),
                                currentCarrierCode.getString("Description"),
                                currentCarrierCode.getDate("updatedAt"));

                        codeList.add(currentCode);
                    }

                    carrierDetailsAdapter.setData(codeList);
                    Logger.d("Got Carrier Codes (%d) for %s", codeList.size(), carrierObjectId);
                } else {
                    Logger.e("Failed to get carriers", e.getMessage());
                }
            }
        });
    }

    private void showAd() {
        mFlurryAdInterstitial.fetchAd();
        // fetch and prepare ad for this ad space. won’t render one yet
        if (mFlurryAdInterstitial.isReady()) {
            mFlurryAdInterstitial.displayAd();
        } else {
            mFlurryAdInterstitial = new FlurryAdInterstitial(this, mAdSpaceName);
            mFlurryAdInterstitial.fetchAd();
        }
    }

    FlurryAdInterstitialListener interstitialAdListener = new FlurryAdInterstitialListener() {

        @Override
        public void onFetched(FlurryAdInterstitial adInterstitial) {
            adInterstitial.displayAd();
        }

        @Override
        public void onError(FlurryAdInterstitial adInterstitial, FlurryAdErrorType adErrorType, int errorCode) {
            adInterstitial.destroy();
        }

        @Override
        public void onRendered(FlurryAdInterstitial flurryAdInterstitial) {

        }

        @Override
        public void onDisplay(FlurryAdInterstitial flurryAdInterstitial) {

        }

        @Override
        public void onClose(FlurryAdInterstitial flurryAdInterstitial) {

        }

        @Override
        public void onAppExit(FlurryAdInterstitial flurryAdInterstitial) {

        }

        @Override
        public void onClicked(FlurryAdInterstitial flurryAdInterstitial) {

        }

        @Override
        public void onVideoCompleted(FlurryAdInterstitial flurryAdInterstitial) {

        }
    };
}
