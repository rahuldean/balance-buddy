package com.godhc.balancebuddy.models;

public class ApplicationError {
    String message;

    public ApplicationError(String message) {
        this.message = message;
    }
}
