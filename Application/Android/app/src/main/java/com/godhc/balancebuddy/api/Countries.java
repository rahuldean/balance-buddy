package com.godhc.balancebuddy.api;

import com.godhc.balancebuddy.models.ApplicationError;
import com.godhc.balancebuddy.models.Country;
import com.orhanobut.logger.Logger;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import static com.godhc.balancebuddy.utils.Constants.ParseClasses.*;
import static com.godhc.balancebuddy.utils.Constants.ParseColumns.*;
import static com.godhc.balancebuddy.utils.Constants.ApplicationErrorMessages.*;

import java.util.ArrayList;
import java.util.List;

/*
 * API that exposes the countries data
 * Since data is obtained asynchronously, we will use callbacks
 */
public class Countries {

    public void getCountries(final DataLoadFinishListener dataLoadFinishListener) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(COUNTRIES);
        query.addAscendingOrder(COUNTRY);
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, com.parse.ParseException e) {
                if (e == null) {
                    List<Country> countries = new ArrayList<>();
                    for (int i = 0; i < list.size(); i++) {
                        ParseObject currentCountry = list.get(i);
                        Country country = new Country(currentCountry.getObjectId(), currentCountry.getString(COUNTRY));
                        countries.add(country);
                    }
                    Logger.d("Got countries(%d) from server/cache", countries.size());
                    dataLoadFinishListener.onDataLoadFinish(countries, null);

                } else {
                    Logger.e(e, e.getMessage());
                    dataLoadFinishListener.onDataLoadFinish(new ArrayList<Country>(), new ApplicationError(FAILED_TO_GET_COUNTRY_DATA));
                }
            }
        });

    }

    public interface DataLoadFinishListener {
        void onDataLoadFinish(List<Country> countryList, ApplicationError error);
    }
}
